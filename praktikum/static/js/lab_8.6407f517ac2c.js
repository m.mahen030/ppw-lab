 // FB initiation function
 window.fbAsyncInit = () => {
   FB.init({
     appId      : '138401630150210',
     cookie     : true,
     xfbml      : true,
     version    : 'v2.11'
   });

   // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
   // dan jalankanlah fungsi render di bawah, dengan parameter true jika
   // status login terkoneksi (connected)

   // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
   // otomatis akan ditampilkan view sudah login
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        render(true);
      }
      else if (response.status === 'not_authorized') {
        render(false);
      }
      else {
        render(false);
      }
  });
 };

 // Call init facebook. default dari facebook
 (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

 // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
 // merender atau membuat tampilan html untuk yang sudah login atau belum
 // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
 // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
 const render = loginFlag => {
   if (loginFlag) {
     // Jika yang akan dirender adalah tampilan sudah login
     // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
     // yang menerima object user sebagai parameter.
     // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
     getUserData(user => {
       // Render tampilan profil, form input post, tombol post status, dan tombol logout
       $('#lab8').html(
         '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<div class="data">' +
              '<table class="table ">'+
                '<thead>'+
                  '<tr>'+
                    '<td><img class="picture" src="'   + user.picture.data.url + '" alt="profpic" /></td>'+
                    '<th colspan="2"><h1>' + user.name + '</h1></th>' +
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                  '<tr>'+
                    '<td><h2> About Me </h2></td>'+ 
                    '<td><h2>' + user.about + '</h2></td>'+ 
                  '</tr>'+
                  '<tr>'+
                    '<td><h3> Birthday </h3></td>' +
                    '<td><h3>' + user.birthday + '</h3></td>' +
                  '</tr>'+
                  '<tr>'+
                    '<td><h3> Email </h3></td>' +
                    '<td><h3>' + user.email + '</h3></td>' +
                  '</tr>'+
                  '<tr>'+
                  '<td><h3> Gender </h3></td>' +
                  '<td><h3>' + user.gender + '</h3></td>' +
                '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>' +
            '<div id="logout-btn">' +
                '<button class="btn btn-danger logout" onclick="facebookLogout()">Logout</button>' +
                '</div>' +
          '</div>' +
          '<input type="text" class="form-control form-control-lg post" id="postInput" placeholder="Whats on your mind,'+ user.first_name +'?">'+
          '<button class="btn btn-primary postStatus" onclick="postStatus()">Post to Facebook</button>'+
          '<div class="feeds">'+
            '<h1>News Feed</h1>'+
          '</div>'+
          '<div class="d-flex flex-xl-column">'
        ); 

       // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
       // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
       // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
       // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
       getUserFeed(feed => {
          feed.data.map(value => {
            if (value.message && value.story) {
              $('div#lab8').append(
                '<div class="p-2">' +
                    '<table class="table">'+
                      '<tr>'+
                          '<p>' + value.message + '</p>' +
                          '<p>' + value.story + '</p>' +
                          '<button class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\') ">Delete</button>'+
                        '</tr>'+
                    '</table>'+
                 '</div>'
              );
            } else if (value.message) {
              $('div#lab8').append(
                '<div class="p-2">' +
                    '<table class="table">'+
                      '<tr>'+
                          '<p>' + value.message + '</p>' +
                          '<button class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\') ">Delete</button>'+
                        '</tr>'+
                    '</table>'+
                '</div>'
              );
            } else if (value.story) {
              $('div#lab8').append(
                '<div class="p-2">' +
                  '<p>' + value.story + '</p>' +
                  '<button class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\')">Delete</button>'+
                '</div>'
              );
            }
          });
        });
      });
    } else {
     $('#lab8').html(
            '<div id="login-btn">' +
            '<button class="loginBtn loginBtn--facebook" onclick="facebookLogin()">Login with Facebook</button>' +
            '</div>'
        );
        $(".status").html("");

    }
  };


 const facebookLogin = () => {
   // TODO: Implement Method Ini
   // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
   // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
   // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function (response) {
      console.log(response);
      render(true);
    }, {scope: 'public_profile,user_posts,publish_actions,user_about_me,email'})
};

 $('#login-button').click(facebookLogin);

 const facebookLogout = () => {
   // TODO: Implement Method Ini
   // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
   // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            FB.logout();
            render(false);
        }
    });
  }

 // TODO: Lengkapi Method Ini
 // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
 // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
 // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
 // meneruskan response yang didapat ke fungsi callback tersebut
 // Apakah yang dimaksud dengan fungsi callback?
 const getUserData = (fun) => {
  FB.getLoginStatus(function(response){
    if (response.status === 'connected') {
      FB.api('/me',{fields :'cover,picture,name,about,birthday,email,gender,first_name'},'GET',
          function(response){
            console.log(response);
            fun(response);
          });
      }
    });
  }

 const getUserFeed = (fun) => {
   // TODO: Implement Method Ini
   // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
   // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
   // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
   // tersebut
   FB.getLoginStatus(function(response){
       if(response.status === 'connected'){
           FB.api("/me/feed",function (response) {
                   if (response && !response.error) {
                       console.log(response);
                       fun(response);
                   }
               });
           }
       });
   };

 const postFeed = (message) => {
   // Todo: Implement method ini,
   // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
   // Melalui API Facebook dengan message yang diterima dari parameter.
   FB.api(
        "/me/feed",
        "POST",
        {
            "message": message
        },
        function (response) {
          if (response && !response.error) {
              console.log('POST ID: ' + response.id);
              render(false);
              render(true);
          }else{
              alert('Error occured');
          }
        }
    );
  };

 const postStatus = () => {
   const message = $('#postInput').val();
   postFeed(message);
 };

const deleteStatus = (id) => {
    var postId = id;
    FB.api(postId, 'delete', function(response) {
      if (!response || response.error) {
        alert('Error occured');
      } else {
        alert('Post was deleted');
        render(false);
        render(true);
      }
    });
  }