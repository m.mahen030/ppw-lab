localStorage.setItem("themes", JSON.stringify([
    {"id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA"},
    {"id": 1, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA"},
    {"id": 2, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA"},
    {"id": 3, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA"},
    {"id": 4, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121"},
    {"id": 5, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121"},
    {"id": 6, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121"},
    {"id": 7, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121"},
    {"id": 8, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121"},
    {"id": 9, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121"},
    {"id": 10, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA"}
]));


localStorage.setItem("selectedTheme", JSON.stringify({"Indigo": {"bcgColor": "#3F51B5", "fontColor": "#FAFAFA"}}));

// Chatbox
var chathead = document.getElementsByClassName('chat-head');
var chatbody = document.getElementsByClassName('chat-body');

$(chathead).click(function(){
    $(chatbody).toggle();
});

$("textarea").keypress(function(e){
      if (e.keyCode == 13 && !e.shiftKey)
      {
          e.preventDefault();
          var txt = $("textarea").val();
          $(".msg-insert").append("<p class='msg-send'>"+txt+"</p>");
        $(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast'); //auto scroll to the latest chat
        $("textarea").val("");
          return;
      }
      else if (e.keyCode == 13 && e.shiftKey)
      {
        e.preventDefault();
          var txt = $("textarea").val();
          $(".msg-insert").append("<p class='msg-receive'>"+txt+"</p>");
        $(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast'); //auto scroll to the latest chat
        $("textarea").val("");
          return;
      }
  });


// END

// Calculator
var print = document.getElementById('print');
var erase = false;

Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};

Math.degrees = function(radians) {
  return radians * 180 / Math.PI;
};

var go = function(x) {
  if (x === 'ac') {
  	print.value = null;
  	erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'sin' || x === 'tan') {
      print.value = Math.round(evil('Math.'+x+'(Math.radians('+evil(print.value)+'))') * 10000) / 10000;
      erase = true;
  } else if (x === 'log') {
      print.value = Math.round((Math.log(evil(print.value)) * 10000) / 10000);
      erase = true;
  } else if (erase === true) {
      print.value = x;
      erase = false;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

$(document).ready(function () {
    $('.my-select').select2({
        data: JSON.parse(localStorage.getItem("themes"))
    });


    $('.apply-button').on('click', function () {
        theme = JSON.parse(localStorage.getItem('themes'))[$('.my-select').val()];
        $('body').css({"backgroundColor": theme['bcgColor']});
        $('.text-center').css({"color": theme['fontColor']});
        localStorage.setItem('selectedTheme', JSON.stringify(theme));
    });


});